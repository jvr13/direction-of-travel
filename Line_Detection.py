#Import Libraries
import cv2
import numpy as np

# load the image, convert it to grayscale, 

img = cv2.imread("line1.jpeg")
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
# perform edge detection
edges = cv2.Canny(gray, 50, 250)
# Detect lines with set Params
lines = cv2.HoughLinesP(edges, 5, np.pi/180, 40, minLineLength= 355, maxLineGap=150)
#initialize list
c_list=[]

#Loop of all lines found
for line in lines:
   x1, y1, x2, y2 = line[0]
   cv2.line(img, (x1, y1), (x2, y2), (0, 0, 128), 5)
   #add cordinate values to the list
   c_list.append(x1)
   c_list.append(y1)
   c_list.append(x2)
   c_list.append(y2)
   
#Find midpoints of 2 lines
x3=int((c_list[4]+c_list[2])/2)
y3=int((c_list[5]+c_list[3])/2)
x4=int((c_list[6]+c_list[0])/2)
y4=int((c_list[7]+c_list[1])/2)
#Start Point of arrow
s_point = (x3,y3)
#End Point of arrow
e_point = (x4,y4)

#Draw Arrow
image= cv2.arrowedLine(img, s_point, e_point, (0, 0, 128), 9)
cv2.imshow("linesDetected", img)
print(c_list)
cv2.waitKey(0)
cv2.destroyAllWindows()